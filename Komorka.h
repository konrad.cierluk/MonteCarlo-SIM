#ifndef KOMORKA_H
#define KOMORKA_H

#include <vector>

class Komorka
{
    public:
        static std::vector <int> N_Kr_F;
        static std::vector <int> N_An_S;

        int poz;
        int nSasadAni;
        int nSasadKr;
        bool wskazOK;

        std::vector<Komorka*> wskaz;

        Komorka();

        void liczSasiadow();
        void aktualizujSasiadow();
        void Reset();
};

#endif // KOMORKA_H
