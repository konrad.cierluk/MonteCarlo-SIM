#ifndef PRAWDOPODOBIENSTWA_H_KC
#define PRAWDOPODOBIENSTWA_H_KC

const double ft = 1;       //parametr z wzoru na częstotliwosc zjawisk

#include <vector>
#include <TRandom3.h>


struct Prawdopodobienstwa
{
    TRandom3* los;
    double alfa;
    double beta;
    double k_an_max;
    double k_kr_max;
    std::vector <double> k_an;
    std::vector <double> k_kr;
    double L_a;
    double L_k;
    std::vector <double> L_an;
    std::vector <double> L_kr;

    Prawdopodobienstwa();
//    Prawdopodobienstwa(double alfa, double beta);
    ~Prawdopodobienstwa();

    void Przelicz();
    void Przelicz(double alfa, double beta);
    bool Kreacja(int liczbaSasiadow);
    bool Anihilacja(int liczbaSasiadow);
};

#endif // PRAWDOPODOBIENSTWA_H_KC
