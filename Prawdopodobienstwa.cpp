#include "Prawdopodobienstwa.h"

#include <algorithm>
#include <cmath>


Prawdopodobienstwa::Prawdopodobienstwa()
{
    k_an.resize(7);
    k_kr.resize(7);
    L_an.resize(7);
    L_kr.resize(7);
    los = new TRandom3();
    los-> SetSeed(0);
    los-> SetSeed(los->GetSeed()+103);
}

//Prawdopodobienstwa::Prawdopodobienstwa(double wczyt_alfa, double wczyt_beta)
//{
//    k_an.resize(7);
//    k_kr.resize(7);
//    L_an.resize(7);
//    L_kr.resize(7);
//    alfa=wczyt_alfa;
//    beta=wczyt_beta;
//    los = new TRandom3();
//    los-> SetSeed(0);
//}


Prawdopodobienstwa::~Prawdopodobienstwa()
{
    delete los;
}

void Prawdopodobienstwa::Przelicz()
{
    for (int i =0; i<7 ; i++)
    {
        k_kr[i]=ft*exp((alfa/6)*(i-3)+beta/2);
        k_an[i]=ft*exp(-(alfa/6)*(i-3)-beta/2);
    }

    k_kr_max=*max_element(k_kr.begin(), k_kr.end());
    k_an_max=*max_element(k_an.begin(), k_an.end());

    for (int i =0; i<7 ; i++)
    {
        L_kr[i] = (k_kr[i]/k_kr_max);
        L_an[i] = (k_an[i]/k_an_max);
    }

    L_k = k_kr_max/(k_kr_max+k_an_max);
    L_a = k_an_max/(k_kr_max+k_an_max);
}

void Prawdopodobienstwa::Przelicz(double wczyt_alfa, double wczyt_beta)
{
    alfa=wczyt_alfa;
    beta=wczyt_beta;
    Przelicz();
}

bool Prawdopodobienstwa::Kreacja(int liczbaSasiadow)
{
    if (los->Rndm()<=L_kr[liczbaSasiadow]) return true;
    else return false;
}


bool Prawdopodobienstwa::Anihilacja(int liczbaSasiadow)
{
    if (los->Rndm()<=L_an[liczbaSasiadow]) return true;
    else return false;
}
