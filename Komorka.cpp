#include "Komorka.h"
#include <iostream>

        std::vector <int> Komorka::N_Kr_F(7);
        std::vector <int> Komorka::N_An_S(7);

Komorka::Komorka()
{
    poz=1;
    nSasadAni=6;
    nSasadKr=0;
    wskazOK=0;
    wskaz.resize(6,0);
}

void Komorka::liczSasiadow()
{
    N_Kr_F[nSasadKr]--;
    N_An_S[nSasadAni]--;

    nSasadAni=0;
    nSasadKr=0;

    for (auto it = wskaz.begin() ;  it != wskaz.end(); ++it)
        {
            if ((*it)->poz > poz) {nSasadKr++; nSasadAni++;}
            else if ((*it)->poz == poz) {nSasadAni++;}
        }

    N_Kr_F[nSasadKr]++;
    N_An_S[nSasadAni]++;
}

void Komorka::aktualizujSasiadow()
{
    liczSasiadow();
    //for (std::vector<Komorka*>::iterator it = wskaz.begin() ;  it != wskaz.end(); ++it)
    for (auto it = wskaz.begin() ;  it != wskaz.end(); ++it)
    {
        (*it)->liczSasiadow();
    }
}

void Komorka::Reset()
{
    poz=1;
    nSasadAni=6;
    nSasadKr=0;
}
