Git global setup

git config --global user.name "Konrad Cierluk"
git config --global user.email "konrad.cierluk@gmail.com"

Create a new repository

git clone git@gitlab.com:konrad.cierluk/MonteCarlo-SIM.git
cd MonteCarlo-SIM
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin git@gitlab.com:konrad.cierluk/MonteCarlo-SIM.git
git add .
git commit
git push -u origin master
