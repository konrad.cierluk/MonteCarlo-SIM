#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <iomanip>

//root
#include <TRandom3.h>
#include <TGraph2D.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TFile.h>
#include <TStopwatch.h>

//project files
#include "Komorka.h"
#include "Prawdopodobienstwa.h"

using namespace std;


//-------------------------------------------------------------------------------
// zmienne globalne
//-------------------------------------------------------------------------------

const string nazwaPlikuZParametrami = "parametry";  //nazwa pliku z parametrami
const string nazwaPlikuZSzybkosciami = "szybkosci"; //nazwa pliku z szybkosciami
const int rozmTab = 32;                             //rozmiar tablicy symulowanej powierzchni

const int nWarstw = 32;          //liczba powstalych warstw do zatrzymania iteracji
const int ilIt = 2000000000;     //liczba iteracji do zakonczenia obliczen


//const double ft = 1;        parametr z wzoru na częstotliwosc zjawisk - w pliku "Prawdopodobienstwa.h"
const double a_dl= sqrt(3);   //parametr do prawidlowego wyswietlania wykresu warstw
const double rT2 = 1.0*rozmTab*rozmTab;     //kwadrat rozmiaru tablicy
const int rTab2 = rozmTab*rozmTab;

int nKreacjiDoKonca=rTab2*nWarstw+rTab2/2;


unsigned long int kreacje, anihilacje, zjawiska, wh, petliSzybkosci, lastZjawisko;
long int kreacjeMinusAnihilacje;
vector <double> szybkosciWzrostu;
double sumSzybkosciWzrostu, sWzr;



//-------------------------------------------------------------------------------
// struktury
//-------------------------------------------------------------------------------

struct parametry {double alfa; double beta; double szybkosc;};

//struct szybkosci {double alfa; double beta; double szybkosc;};

//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
// funkcje
//-------------------------------------------------------------------------------

void zaadresujSasiadow(Komorka[rozmTab][rozmTab]);
void resetTablicy(Komorka[rozmTab][rozmTab]);
vector <parametry> odczytPlikuZParametrami(string);
void zapisPlikuZPredkosciami(string, vector <parametry> );
void odczytPlikuZSzybkosciami(string, vector <parametry> &);
TGraph2D * zwrocTGraph(Komorka[rozmTab][rozmTab]);
TGraph2D* rysujWykresSzybkosci(vector <parametry>);
double szybkoscWzrostu(Prawdopodobienstwa*);
bool warunek();
void resetParametrowPetli();
void PrintTime(double);
double GetTime(double, int &, int &);

//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//                               PROGRAM GLOWNY
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


int main(int argc, char* argv[])
{

//-------------------------- zmienne globalne symulacji -------------------------
//-------------------------------------------------------------------------------

    int iTab, jTab;
    double los;

 //   TGraph * wykresSzybkosci = new TGraph();

    TRandom3* iTabRand = new TRandom3();
    TRandom3* jTabRand = new TRandom3();
    TRandom3* lRand = new TRandom3();

    TStopwatch *watch = new TStopwatch();
    double zegarek;

    TApplication* rootapp = new TApplication("example",&argc, argv);

    //Prawdopodobienstwa L;   //prawdopodobienstwa
    Prawdopodobienstwa * L = new Prawdopodobienstwa();


//-------------------------------------------------------------------------------
    //uzaleznienie losowania od czasu procesora

        iTabRand->SetSeed(0);
        jTabRand->SetSeed(iTabRand->GetSeed()+1);
        lRand->SetSeed(iTabRand->GetSeed()+2);

        TGraph2D *szybkosciWykres;
        TGraph2D *graph;
        TCanvas *c = new TCanvas("sym_MC","Symulacja MonteCarlo",0,0,600,800);



//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//------------------- tworzenie tablicy obiektów klasy Komorka ------------------
//------------------------- oraz zaadresowanie sąsiadów -------------------------

    Komorka tablica[rozmTab][rozmTab];      //tworzenie tablicy obiektow - komorek
    zaadresujSasiadow(tablica);             //zaadresowanie sasiadow dla kazdej komorki
    vector <parametry> vecParam = odczytPlikuZParametrami(nazwaPlikuZParametrami);  //odczyt parametrow


    TFile*  f_temp = new TFile("rootfiles/wykresy.root","recreate");

    fstream txt_log;
    txt_log.open( "log", std::ios::out | ios::app );
    int log_h;
    int log_min;
    double log_sek;

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


//---------------------------GŁÓWNA PĘTLA PROGRAMU-------------------------------
//-------------------------------------------------------------------------------

for (vector<parametry>::iterator p = vecParam.begin() ; p != vecParam.end(); ++p)
{
    watch->Start(1);
//----------------------------------
cout <<"Rozpoczecie symulacji dla \talfa = "<< p->alfa <<"\tbeta = "<< p->beta<<endl;
txt_log << "\n----------------********************----------------\n\n";
txt_log << "Rozpoczecie symulacji dla \talfa = "<< p->alfa <<"\tbeta = "<< p->beta<<endl;
//----------------------------------

    resetTablicy(tablica);                  //Przywrocenie wartosci domyslnych dla tablicy obiektow
    L->Przelicz(p->alfa,p->beta);            //Przeliczenie dla konkretnych alfa i beta
    resetParametrowPetli();

    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------


    //----------------------------- PĘTLA MNIEJSZA ----------------------------------
    //-------------------------------------------------------------------------------


    while (warunek())
    {
    //------------------------------------------
    //------zmienne losowe - losowanie ---------
            iTab = iTabRand -> Integer(rozmTab);
            jTab = jTabRand -> Integer(rozmTab);
            los = lRand -> Rndm();
    //------------------------------------------
    //------------------------------------------


        //------------------------kreacja
            if (los <= L->L_k)
            {
                if (L->Kreacja(tablica[iTab][jTab].nSasadKr))
                {
                    kreacje++;
                    kreacjeMinusAnihilacje++;
                    zjawiska++;
                    tablica[iTab][jTab].poz++;
                    tablica[iTab][jTab].aktualizujSasiadow();
                }
            }
        //------------------------anihilacja
            else
            {
                if (L->Anihilacja(tablica[iTab][jTab].nSasadAni))
                {
                    anihilacje++;
                    kreacjeMinusAnihilacje--;
                    zjawiska++;
                    tablica[iTab][jTab].poz--;
                    tablica[iTab][jTab].aktualizujSasiadow();
                }
            }
    wh++;


    if (kreacjeMinusAnihilacje>rTab2)
        {
        if (zjawiska!=lastZjawisko)
        {
            //if (zjawiska%2048==0)
            //{
            lastZjawisko=zjawiska;
            petliSzybkosci++;
            sWzr=szybkoscWzrostu(L);
//            wykresSzybkosci->SetPoint(petliSzybkosci,petliSzybkosci,sWzr);
            szybkosciWzrostu.push_back(sWzr);
            sumSzybkosciWzrostu+=sWzr;
            //}
        }
        }
    }

//uzupelnianie szybkosci wzrostu
if (szybkosciWzrostu.size()==0) p->szybkosc=0;
else p->szybkosc=sumSzybkosciWzrostu/szybkosciWzrostu.size();

graph = zwrocTGraph(tablica);
//graph ->SetName();


//graph->Draw("PCOL");
//rysunek->SetDrawOption("PCOL");
//graph->Write();
//graph->Delete();
//graph-> Draw("pcol");

//wykresSzybkosci->Draw();

cout<<"\nkreacjeMinusAnihilacje:\t"<<kreacjeMinusAnihilacje<<endl;
cout<<"kreacje:\t"<<kreacje<<endl;
cout<<"anihilacje:\t"<<anihilacje<<endl;
cout<<"zjawiska:\t"<<zjawiska<<endl<<endl;
cout<<"ilosc petli:\t"<<wh<<endl<<endl;
cout<<"szybkosc wzrostu:\t"<<sumSzybkosciWzrostu/szybkosciWzrostu.size()<<endl<<endl;
cout<<"Czas symulacji: ";
PrintTime( (double) watch-> RealTime() );
cout<<endl<<endl;

txt_log<<"\nKreacje minus Anihilacje:\t"<<kreacjeMinusAnihilacje<<endl;
txt_log<<"Ilosc kreacji:\t\t"<<kreacje<<endl;
txt_log<<"Ilosc anihilacji:\t"<<anihilacje<<endl;
txt_log<<"Ilosc zjawisk:\t\t"<<zjawiska<<endl;
txt_log<<"Ilosc petli:\t\t"<<wh<<endl;
txt_log<<"Czas symulacji:\t\t";
log_sek=GetTime((double) watch-> RealTime(),log_h,log_min);
if (log_h != 0) txt_log << log_h <<" godz. "; if(log_min!=0) txt_log <<" min. "; txt_log<< log_sek <<" s."<<endl<<endl;
txt_log<<"Szybkosc wzrostu:\t"<<sumSzybkosciWzrostu/szybkosciWzrostu.size()<<endl;
txt_log << "\n----------------------------------------------------\n";

zapisPlikuZPredkosciami("szybkosci", vecParam);


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



}//-END---GŁÓWNA PĘTLA PROGRAMU-------------------------------------------------

txt_log.close();

odczytPlikuZSzybkosciami("szybkosci2", vecParam);

szybkosciWykres=rysujWykresSzybkosci(vecParam);
szybkosciWykres->Draw("PCOL");
szybkosciWykres->Write();

rootapp->Run();

    cout << "Hello world!" << endl;

return 0;
}







//-------------------------------------------------------------------------------
//------  Reset parametrow petli  -----------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void resetParametrowPetli()
{
petliSzybkosci=0;
wh=0;
kreacje=0;
anihilacje=0;
kreacjeMinusAnihilacje=0;
zjawiska=0;
szybkosciWzrostu.clear();
sumSzybkosciWzrostu=0;
lastZjawisko=0;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Warunek wykonywania iteracji  -----------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

bool warunek()
{
if ((wh < ilIt) && (abs(kreacjeMinusAnihilacje) < nKreacjiDoKonca)) return true;
else return false;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Funkcja adresowania sasiadow  -----------------------------------------
//-------------------------------------------------------------------------------
//  dla tablicy kwadratowej

void zaadresujSasiadow(Komorka tab2dim[rozmTab][rozmTab])
{
    for (int i=0;i<rozmTab;i++)
    {
        for (int j=0;j<rozmTab;j++)
        {
        if (j==0) { tab2dim[i][j].wskaz[0] = & tab2dim[i][rozmTab-1];} else { tab2dim[i][j].wskaz[0] = & tab2dim[i][j-1];}
        if (j==rozmTab-1) { tab2dim[i][j].wskaz[1] = & tab2dim[i][0];} else { tab2dim[i][j].wskaz[1] = & tab2dim[i][j+1];}
        if (i==0) { tab2dim[i][j].wskaz[2] = & tab2dim[rozmTab-1][j];} else { tab2dim[i][j].wskaz[2] = & tab2dim[i-1][j];}
        if (i==rozmTab-1) { tab2dim[i][j].wskaz[3] = & tab2dim[0][j];} else { tab2dim[i][j].wskaz[3] = & tab2dim[i+1][j];}

        if (i%2==0)
            {
                if (j==0)
                    {
                        if (i==0) { tab2dim[i][j].wskaz[4] = & tab2dim[rozmTab-1][rozmTab-1];}
                        else { tab2dim[i][j].wskaz[4] = & tab2dim[i-1][rozmTab-1];}

                        if (i==rozmTab-1) { tab2dim[i][j].wskaz[5] = & tab2dim[0][rozmTab-1];}
                        else { tab2dim[i][j].wskaz[5] = & tab2dim[i+1][rozmTab-1];}
                    }
                else
                    {
                        if (i==0) tab2dim[i][j].wskaz[4] = & tab2dim[rozmTab-1][j-1];
                        else tab2dim[i][j].wskaz[4] = & tab2dim[i-1][j-1];

                        if (i==rozmTab-1) tab2dim[i][j].wskaz[5] = & tab2dim[0][j-1];
                        else tab2dim[i][j].wskaz[5] = & tab2dim[i+1][j-1];
                    }
            }

            else
            {
                if (j==rozmTab-1)
                    {
                        tab2dim[i][j].wskaz[4] = & tab2dim[i-1][0];

                        if (i==rozmTab-1) { tab2dim[i][j].wskaz[5] = & tab2dim[0][0];}
                        else { tab2dim[i][j].wskaz[5] = & tab2dim[i+1][0];}
                    }
                else
                    {
                        tab2dim[i][j].wskaz[4] = & tab2dim[i-1][j+1];

                        if (i==rozmTab-1) tab2dim[i][j].wskaz[5] = & tab2dim[0][j+1];
                        else tab2dim[i][j].wskaz[5] = & tab2dim[i+1][j+1];
                    }
            }
        tab2dim[i][j].wskazOK=1;
        }
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Funkcja resetujaca poziom warst  --------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


void resetTablicy(Komorka tab2dim[rozmTab][rozmTab])
{
    for (int i=0;i<rozmTab;i++)
    {
        for (int j=0;j<rozmTab;j++) tab2dim[i][j].Reset();
    }

    //reset zmiennych statycznych klasy Komorka
    for (int i=0;i<7;i++)
    {
        Komorka::N_An_S[i]=0;
        Komorka::N_Kr_F[i]=0;
    }
    Komorka::N_An_S[6]=(rozmTab*rozmTab);
    Komorka::N_Kr_F[0]=(rozmTab*rozmTab);

}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------




//-------------------------------------------------------------------------------
//------  Funkcja liczaca predkosc wzrostu  -------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


double szybkoscWzrostu(Prawdopodobienstwa* l)
{
    int suma=0;
    for (int i=0;i<7;i++)
    {
        suma+= (l->k_kr[i]*Komorka::N_Kr_F[i]-l->k_an[i]*Komorka::N_An_S[i]);
    }
    return suma/rT2;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Funkcja tworzaca wykres wytworzonej powierzchni  ----------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

TGraph2D* zwrocTGraph(Komorka tab2dim[rozmTab][rozmTab])
{
TGraph2D *gr = new TGraph2D();
int Nr = 0;

    for (int i=0;i<rozmTab;i++)
    {
        if (i%2==0){
            for (int j=0;j<rozmTab;j++)
            {
                gr->SetPoint(Nr,i*a_dl,(j+0.5),tab2dim[i][j].poz);
                Nr++;
            }
        }
        else
        {
            for (int j=0;j<rozmTab;j++)
            {
                gr->SetPoint(Nr,i*a_dl,j,tab2dim[i][j].poz);
                Nr++;
            }
        }
    }
    gr->SetMarkerStyle(20);
    gr->SetTitle("Warstwy w krysztale");

    return gr;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Funkcja odczytu parametrow alfa i beta z pliku  -----------------------
//----------------oraz zaladowanie ich do vectora -------------------------------
//-------------------------------------------------------------------------------

vector <parametry> odczytPlikuZParametrami(string nazwaPliku)
{
    fstream plikParam;
    string linia;
    stringstream buf;
    parametry param;
    vector <parametry> vecPar;

    plikParam.open( nazwaPliku.c_str(), ios::in);
    if( plikParam.good() == true )
    {
        getline(plikParam, linia);
        while(linia != "")              //przerwij jeżeli linia będzie pusta (dane w pliku się skończą)
        {
            buf << linia;
            buf >> param.alfa >> param.beta;
            buf.clear();
            vecPar.push_back(param);
            getline(plikParam, linia);
        }
        plikParam.close();
    }
    return vecPar;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
//------  Funkcja zapisu predkosci wzrostu do pliku  ----------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void zapisPlikuZPredkosciami(string nazwaPliku, vector <parametry> vPar)
{
    fstream plikSzybk;
    plikSzybk.open( nazwaPliku.c_str(), ios::out);

    for (vector<parametry>::iterator it = vPar.begin() ; it != vPar.end(); ++it)
        {
            plikSzybk<<it->alfa<<"\t"<<it->beta<<"\t"<<it->szybkosc<<endl;
        }
    plikSzybk.close();

}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//------  Funkcja odczytu parametrow alfa i beta i szybkosci  -------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void odczytPlikuZSzybkosciami(string nazwaPliku, vector <parametry> &vecPar)
{
    fstream plikParam;
    string linia;
    stringstream buf;
    parametry param;

    plikParam.open( nazwaPliku.c_str(), ios::in);
    if( plikParam.good() == true )
    {
        getline(plikParam, linia);
        while(linia != "")              //przerwij jeżeli linia będzie pusta (dane w pliku się skończą)
        {
            buf << linia;
            buf >> param.alfa >> param.beta>>param.szybkosc;
            buf.clear();
            vecPar.push_back(param);
            getline(plikParam, linia);
        }
        plikParam.close();
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


















TGraph2D* rysujWykresSzybkosci(vector <parametry> vPar)
{
TGraph2D *gr = new TGraph2D();
int Nr = 0;

for (vector<parametry>::iterator it = vPar.begin() ; it != vPar.end(); ++it)
        {
            gr->SetPoint( Nr , it->alfa, it->beta, it->szybkosc);
            Nr++;
        }
    gr->SetMarkerStyle(20);
    gr->SetTitle("Wykres Szybkosci Wzrostu Krysztalow");

    return gr;
}

void PrintTime(double sekonds)
{
    int minut;
    int hour;

    minut = (int) sekonds / 60;
    sekonds = sekonds - minut * 60.0;
    hour = minut / 60;
    minut = minut - hour * 60;

    if ((hour!=0)&&(minut!=0)) cout<<" "<<hour<<" h  "<<minut<<" min  "<<sekonds<<" s";
    else if (minut!=0) cout<<" "<<minut<<" min  "<<sekonds<<" s";
    else cout<<" "<<sekonds<<" s";
}

double GetTime(double sekonds, int &godzin, int &minut)
{
    minut = (int) sekonds / 60;
    double sekund = sekonds - minut * 60.0;
    godzin = minut / 60;
    minut = minut - godzin * 60;

    return sekund;
}
